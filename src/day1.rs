use std::fs::File;
use std::io::{BufRead, BufReader, Result};
use regex::Regex;

fn word_to_digit(word: &str) -> Option<u32> {
    match word {
        "one" => Some(1),
        "two" => Some(2),
        "three" => Some(3),
        "four" => Some(4),
        "five" => Some(5),
        "six" => Some(6),
        "seven" => Some(7),
        "eight" => Some(8),
        "nine" => Some(9),
        "1" => Some(1),
        "2" => Some(2),
        "3" => Some(3),
        "4" => Some(4),
        "5" => Some(5),
        "6" => Some(6),
        "7" => Some(7),
        "8" => Some(8),
        "9" => Some(9),
        "fiveight" => Some(8),
        "twone" => Some(1),
        "eightwo" => Some(2),
        "nineight" => Some(8),
        "eighthree" => Some(3),
        "oneight" => Some(8),
        _ => None,
    }
}

fn find_first_digit_or_number(input: &str) -> Option<u32> {
    let re = Regex::new(r"\d|one|two|three|four|five|six|seven|eight|nine").unwrap();
    if let Some(mat) = re.find(input) {
        let the_str = mat.as_str();
        word_to_digit(the_str)
    } else {
        None
    }
}

fn find_last_digit_or_number(input: &str) -> Option<u32> {
    let re = Regex::new(r"\d|oneight|twone|eightwo|fiveight|nineight|eighthree|one|two|three|four|five|six|seven|eight|nine").unwrap();
    let mut last_match: Option<u32> = None;

    for matched in re.find_iter(input).map(|m| m.as_str()) {
        if let Some(digit) = word_to_digit(matched) {
            last_match = Some(digit);
        }
    }

    last_match
}


fn find_digits(input: &str) -> u32 {
    let first_digit = find_first_digit_or_number(&input);
    let last_digit = find_last_digit_or_number(&input);

    if let (Some(first), Some(last)) = (first_digit, last_digit) {
        let my_num = first *10 + last;
        return my_num;
    }
    0
}

pub fn parse_file(file_path: &str) -> Result<()> {
    let file = File::open(file_path)?;
    let reader = BufReader::new(file);
    let mut sum = 0;

    for line in reader.lines() {
        let line_contents = line?;
        let num = find_digits(&line_contents);
        sum += num;
    }
    println!("The correct calibration is: {}.", sum);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_last_digit_or_number() {
        let text = "3kbqbzm2sixnine52onekrtoneighttq";
        assert_eq!(find_last_digit_or_number(text), Some(8));
    }
    #[test]
    fn test_find_first_one() {
        let text = "krtoneighttq";
        assert_eq!(find_first_digit_or_number(text), Some(1));
    }
    #[test]
    fn test_find_last_digit_or_number_empty() {
        let text = "No digits here";
        assert_eq!(find_last_digit_or_number(text), None);
    }
    #[test]
    fn test_last_is_three() {
        let text = "njpnzndmdfzkpdseven5rvcsxheighthree";
        assert_eq!(find_last_digit_or_number(text), Some(3));
    }
    #[test]
    fn test_get_calibration() {
        let text = "6ninefive";
        assert_eq!(find_digits(text), 65);
    }
    #[test]
    fn test_another_input() {
        let text = "gn7oneeightzcshvfttpnvxrxjdc";
        assert_eq!(find_digits(text), 78);
    }
    #[test]
    fn test_one_digit() {
        let text = "gfjxcczktc8";
        assert_eq!(find_digits(text), 88);
    }
    #[test]
    fn test_weird_combo() {
        let text = "threevrvvc2eightwo";
        assert_eq!(find_digits(text),32); 
    }
    #[test]
    fn test_another_combo() {
        let text = "fiveight";
        assert_eq!(find_digits(text), 58);
    }
}
