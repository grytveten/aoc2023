use std::fs::File;
use std::io::{BufRead, BufReader, Result};
use regex::Regex;


fn find_game_id(input: &str) -> Option<u32> {
    let game_id_re = Regex::new(r"\d+").unwrap();
    if let Some(matched) = game_id_re.find(input) {
        let game_id_str = matched.as_str();
        let game_id: Option<u32> = game_id_str.parse().ok();
        return game_id;
    } else {
        println!("No match found");
        return Some(0);
    }
}

fn check_game(input: &str, num_blue: u32, num_green: u32, num_red: u32) -> bool {
    let re = Regex::new(r"(\d+)\s*(green|red|blue)").expect("Invalid regex pattern");
    for cap in re.captures_iter(input) {
        let number: Option<u32> = cap.get(1).unwrap().as_str().parse().ok();
        let color = cap.get(2).unwrap().as_str();
        
        if let Some(col_num) = number {
              if color == "red" && col_num > num_red {return false;}
              if color == "blue" && col_num > num_blue {return false;}
              if color == "green" && col_num > num_green {return false;}
        }
    }
    return true;
    }

fn is_it_possible(input: &str, num_blue: u32, num_green: u32, num_red: u32) -> u32 {
    let game_id = find_game_id(&input);
    let valid_game = check_game(&input, num_blue, num_green, num_red);
    if valid_game {    
        if let Some(good_game_id) = game_id {
            return good_game_id;
        }
    }
    0
}

fn parse_and_check_file(file_path: &str, num_blue: u32, num_green: u32, num_red: u32) -> Result<()> {
    let file = File::open(file_path)?;
    let reader = BufReader::new(file);
    let mut sum = 0;

    for line in reader.lines() {
        let line_contents = line?;
        let num = is_it_possible(&line_contents, num_blue, num_green, num_red);
        sum += num;
    }
    println!("The sum of IDs of OK games is: {}.", sum);

    Ok(())
}


pub fn sum_ids(num_blue: u32, num_green: u32, num_red: u32, input_file: &str) -> Result<()> {
    println!(
        "Finding the sum of the possible games with {} blue, {} green, and {} red cubes:",
        num_blue, num_green, num_red
    );

    let file_result = parse_and_check_file(&input_file, num_blue, num_green, num_red);
 
    return file_result;
}

fn find_set_power(input: &str) -> u32 {
   let re = Regex::new(r"(\d+)\s*(green|red|blue)").expect("Invalid regex pattern");
   let mut num_blue = 1;
   let mut num_green = 1;
   let mut num_red = 1;
   for cap in re.captures_iter(input) {
        let number: Option<u32> = cap.get(1).unwrap().as_str().parse().ok();
        let color = cap.get(2).unwrap().as_str();

        if let Some(col_num) = number {
              if color == "red" && col_num > num_red {num_red = col_num;}
              if color == "blue" && col_num > num_blue {num_blue = col_num;}
              if color == "green" && col_num > num_green {num_green = col_num;}
        }
    }
   return num_blue * num_green * num_red;
}

pub fn find_sum_set_power(file_path: &str) -> Result<()> {
    let file = File::open(file_path)?;
    let reader = BufReader::new(file);
    let mut sum = 0;

    for line in reader.lines() {
        let line_contents = line?;
        let num = find_set_power(&line_contents);
        sum += num;
    }
    println!("The sum of the power of the sets is: {}.", sum);

    Ok(())
}