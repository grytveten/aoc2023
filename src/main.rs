mod day1;
mod day2;
use day1::parse_file;
use day2::sum_ids;
use day2::find_sum_set_power;


fn main() {
    println!("Solving day 2");
    println!("I thought we were in a hurry to get snow, but seems we are playing games now:");
    let num_blue = 14;
    let num_green = 13;
    let num_red = 12;

    println!("Solving first part of day 2:");
    if let Err(err) = sum_ids(num_blue, num_green, num_red, "input_day2.txt") {
        eprintln!("Error; {:?}", err);    
    }
    println!("Solving the second part of day 2:");
    if let Err(err) = find_sum_set_power("input_day2.txt") {
        eprintln!("Error; {:?}", err);
    }
    println!("Solving day 1: ");
    if let Err(err) = parse_file("input_day1.txt") {
        eprintln!("Error: {:?}", err);
    }
}

